# DevOpsをGitFlowと繋げて実施する

- より実務に近い形のDevOpsを体験する

## CIのセットアップ

- CIからDockerイメージをPushするためのサービスアカウントを生成

```
$ gcloud iam service-accounts create ci-account \
    --display-name ci-account
```

- 後のコマンドで使用するために、サービス アカウントのメールアドレスと現在のプロジェクト ID を環境変数に格納します。

```
$ export CI_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:ci-account" \
    --format='value(email)') && \
  export PROJECT=$(gcloud info --format='value(config.project)') && \
  echo $CI_EMAIL $PROJECT
```

- editor 役割をサービス アカウントにバインドします。

```
$ gcloud projects add-iam-policy-binding \
    $PROJECT --role roles/editor --member serviceAccount:$CI_EMAIL
```

- サービス アカウントキーをホームディレクトリにダウンロードしておきます

```
$ cd && \
  gcloud iam service-accounts keys create ci-sa.json --iam-account $CI_EMAIL
```

- CIに設定する情報を確認します

```
$ export PROJECT=$(gcloud info --format='value(config.project)') && \
  echo $PROJECT && \
  cat ci-sa.json
```

- CI (GitLab CI) の設定に環境変数を設定します
  - GCLOUD_SERVICE_KEY : ci-sa.json の内容
  - PROJECT_ID_PRODUCTION : $PROJECTの値(GCPのプロジェクト名)
- GitLabのUIで Settings > CI / CD を選択

<img src="image/gitlab-ci-menu.png" width="30%" />

- 上記の環境変数をセットして Save variables で保存

<img src="image/gitlab-ci-variables.png" width="80%" />


## GitFlowでアプリの更新

- GitLabで新しいIssueを発行して、その内容に沿った対応をします
- Issueを「Display date and time」というタイトルで作成し、 Merge Request を発行します
  - 「Create merge request」 ボタンで、そのIssueを対応するためのブランチと、それをレビューするためのMRが作成されます
- 対応するブランチを checkout　して、画面に日時を表示するための対応を行います

- Issueを新規作成

<img src="image/gitlab-new-issue.png" width="80%" />

- 「Create merge request」を実行

<img src="image/gitlab-create-mr.png" width="80%" />


- 2章でローカルにクローンした環境で、上で作成したブランチ（ブランチ名にIssueの番号がついています）をチェックアウトして編集します
- `web/public/index.html` の 70行目を以下のように変更する
  - 日時は実際にこの変更をした日時を設定すると、それっぽいです

```
-           version: 'v0.0.1',
+           version: 'v2.0.0' + ' - 2019/10/25 13:20',
```

- CIでは、HTMLのLintチェックのみ実施しています
  - 今回はテストコードは対象外
- CIがエラーになることを試すため、`web/public/index.html` の 5行目を以下のように変更する
  - 使っているLintのルールではHTMLのタグは小文字というルールがあるため、動作上は問題ないけれどCIは失敗する

```
-     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
+     <META name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
```

- ローカルで変更した内容を確認(Skaffold を起動)
- http://localhost で変更した内容を目視確認

```
$ skaffold dev
```

- 変更した内容をコミットしてPush

## GitLabで Merge Request で CI の結果を確認

- CIは失敗している

<img src="image/gitlab-ci-fail.png" width="70%" />


- CIのpiplineの詳細を見ると、HTMLのタグに問題があるのがわかる

![](image/gitlab-ci-detail.png)



## CI が成功するようにコードを修正

- CIがエラーになる部分を元に戻す
  - `web/public/index.html` の 5行目を以下のように変更する

```
-     <META name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
+     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
```


## GitLabで Merge Request で CI の結果を確認

- CIが成功する

<img src="image/gitlab-ci-success.png" width="70%" />


- Merge Request は 「Create merge request」 ボタンで作成したものは、WIP(Work In Progress)になっており、Mergeボタンが押せない
- Merge Request を Edit し WIP を解除する

<img src="image/gitlab-remove-wip.png" width="70%" />

- マージする


## CDを試す

- CIで確認し、更新したアプリをCDにつなげる
- CDのパイプラインは以下のように進む
  - GitLab CIが Docker イメージをビルドする
  - GitLab CIは ビルドしたイメージを Container Registry にプッシュする
  - Container Registry にプッシュされたイベントを元に、Spinnakerが起動してカナリアにデプロイ
  - カナリア確認後、Spinnakerで本番環境にデプロイ
- CDのパイプラインのトリガは、GitLabでタグを打つと開始されるので、`v2.0.0` のタグを打つ

- メニューの Tags を選択

<img src="image/gitlab-tags.png" width="70%" />

- `v2.0.0` のタグを登録

<img src="image/gitlab-new-tag.png" width="70%" />

- タグを登録すると、自動でGitLabのCIのパイプラインが起動する

<img src="image/gitlab-ci-by-tag.png" width="70%" />

- パイプラインが成功し、Dockerイメージをビルド＆Container Registryに登録する

<img src="image/gitlab-push-success.png" width="70%" />

- GCP の Container Registry に `v2.0.0` のイメージが登録されている
- Spinnakerが `v2.0.0` のイメージをカナリアにデプロイしている
  - バージョンの隣に、対応した日時が表示されている！
- Spinnaker で本番環境へ反映



**おつかれさまでした。これでCI/CDが揃ったモダンな開発・運用環境が揃いました。**
**クリーンアップに進みます**

